import numpy as np
import pickle
import networkx as nx

def get_matrix_for_nx(matrix):
    matrix_nx = [[0 for i in range(len(matrix))] for j in range(len(matrix))]
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            for m in matrix[i][j]:
                matrix_nx[i][m] = 1
    return np.array(matrix_nx)

def unpickle_from_file(file_name):
    with open(file_name, 'rb') as file:
        data = pickle.load(file)
    return data

sparse_clash_matrix = unpickle_from_file(f"clash_matrix.pick")
clash_matrix_nx = get_matrix_for_nx(sparse_clash_matrix)
G_clash = nx.from_numpy_array(clash_matrix_nx, create_using= nx.Graph)

print(G_clash.number_of_edges())
