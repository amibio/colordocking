import numpy as np
import pickle 
import pandas as pd
import matplotlib.pyplot as plt

def unpickle_from_file(file_name):
    with open(file_name, 'rb') as file:
        data = pickle.load(file)
    return data

com_array = np.load('coords_com.npy')
clash_matrix = unpickle_from_file('clash_matrix.pick')

distrib = []

for i in range(len(clash_matrix)):
    for j in clash_matrix[i][0]:
        if j != i:
            com = com_array[i][j]
            distrib.append(com)


df = pd.DataFrame(distrib)
#df.to_csv('distribution_clashes.tsv',sep='\t')
ax = df.plot.bar()
plt.show()
