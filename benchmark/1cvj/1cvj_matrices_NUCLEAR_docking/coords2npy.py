import numpy as np
import pickle 

def unpickle_from_file(file_name):
    with open(file_name, 'rb') as file:
        data = pickle.load(file)
    return data


coords = unpickle_from_file('coords.pick')
babel = unpickle_from_file('babel.pick')
crds = unpickle_from_file('crd_files.pick')

coords_poses = []

for i in range(len(crds)):
    group = crds[i]
    id_pose = babel[i]-1
    coords_poses.append(coords[group][id_pose])

coords_poses_np = np.array(coords_poses)
np.save('coords.npy',coords_poses_np)
