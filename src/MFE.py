import colorsOp as cOp
import math
import pandas as pd
import sys
import time
import MFEc

'''
FILE    MFE.py
DESCR   Python Library of Probabilistic functions to compute the MFE hard-clashfree or Subopts soft-clashfree with MFE library in C.     
'''

def existing_selfavoiding_path_cliques_rec(i, p, G_cliques, k, colors):
    if i == k:
        return True
    for vp in G_cliques.neighbors(p[-1]):
        if colors[vp] == i and vp not in p:
            path = existing_selfavoiding_path_cliques_rec(i+1, p+[vp], G_cliques, k, colors)
            if path == True:
                return path 
    return False

def existing_selfavoiding_path_cliques(G_cliques, k, colors):
    path = False
    for v in G_cliques.nodes():
        if colors[v] == 0:
            path = existing_selfavoiding_path_cliques_rec(1, [v], G_cliques, k, colors)
            if path == True:
                break
    return path

def MFE_parser(link_matrix, classifiedNodes, k, nodes_weight, clash_matrix, nucleotides, sequence, encodedSequence, max_subopts):
    MFEc.ParserPy2C(link_matrix, classifiedNodes, k, nodes_weight, clash_matrix, nucleotides, sequence, encodedSequence, max_subopts)


def MFE_MemoryRelease():
    MFEc.MemoryRelease()


def MFE_Probabilistic_C(link_matrix, G_cliques, k, cliques, nucleotides, babel, epsilon, rnd, output_dir, step_export, use_cliques=True):
    print("Execution MFE")
    p = 1 - (1/math.pow(k,k))
    M = max(int((math.log(epsilon))/(math.log(p))), 1)
    min_score = sys.float_info.max
    min_path_nodes = []
    times = {"MFE":[], "CliquePaths":[0]}
    temp_log = {"Iterations/TotalIterations":[]}
    step_export_init = step_export
    selfavoidingPath_inCliques = False
    for i in range(0,M):
        colors_nodes, colors_cliques = cOp.random_colors(len(link_matrix), k, rnd, cliques, use_cliques)
        if use_cliques == True:
            start_time = time.process_time()
            selfavoidingPath_inCliques = existing_selfavoiding_path_cliques(G_cliques, k, colors_cliques)
            end_time = time.process_time()
            times["CliquePaths"].append((end_time - start_time))
        if selfavoidingPath_inCliques == True or use_cliques == False:
            start_time = time.process_time()
            mfe = MFEc.MFE(colors_nodes)
            end_time = time.process_time()
            times["MFE"].append((end_time - start_time))
            if mfe[1] < min_score:
                min_score = mfe[1]
                min_path_nodes = mfe[0] 
        if i == step_export:
            temp_log["Iterations/TotalIterations"].append(f"{i}/{M}")
            df_temp_log = pd.DataFrame.from_dict(temp_log)
            df_temp_log.to_csv(f"{output_dir}/{k}nt_MFE_temp.log", sep="\t")
            step_export += step_export_init
    min_seq = [nucleotides[i] for i in min_path_nodes]
    min_path = [babel[i] for i in min_path_nodes]
    df_temp_out = pd.DataFrame([(tuple(min_seq), tuple(min_path), round(min_score,6), tuple(min_path_nodes), M, "Hard_Clashfree")], columns = ['Sequence','PoseSeq','EnergySeq','Path_GraphNodeIDs','TotalNbColoring','Clash_Type'])
    df_temp_out.to_csv(f"{output_dir}/{k}nt_MFE_temp.out", sep="\t")
    return (sum(times["MFE"]),sum(times["CliquePaths"])), (tuple(min_seq), tuple(min_path), round(min_score,6), tuple(min_path_nodes), M, "Hard_Clashfree")


def Subopts_Probabilistic_C(link_matrix, G_cliques, k, cliques, nucleotides, babel, delta_max, step, epsilon, max_subopts, rnd, output_dir, step_export, use_cliques=True): 
    print("Execution Subopts")
    p = 1 - (1/math.pow(k,k))
    M = max(int((math.log(epsilon))/(math.log(p))), 1)
    subopts = []
    subopts_temp = []
    Emax = sys.float_info.max
    abs_nb_paths_rejected = 0
    abs_nb_path_clashfree = 0
    step_export_init = step_export
    selfavoidingPath_inCliques = False
    times = {"Subopts":[], "Delta":[[] for i in range(M)], "CliquePaths":[0]}
    temp_log = {"Iterations/TotalIterations":[], "Number of Subopts":[]}
    delta_log = {"NumberSuboptsPerDelta":[[] for i in range(M)]}
    for i in range(0,M):
        print(i)
        colors_nodes, colors_cliques = cOp.random_colors(len(link_matrix), k, rnd, cliques, use_cliques)
        delta = 0
        if use_cliques == True:
            start_time = time.process_time()
            selfavoidingPath_inCliques = existing_selfavoiding_path_cliques(G_cliques, k, colors_cliques)
            end_time = time.process_time()
            times["CliquePaths"].append((end_time - start_time))
        if selfavoidingPath_inCliques == True or use_cliques == False:
            Emfe = MFEc.MFE(colors_nodes)[1]
            delta = step
            while delta <= Emax-Emfe+step:
                start_time = time.process_time()
                subopts_i = MFEc.Subopts(colors_nodes, delta)
                end_time = time.process_time()
                times["Subopts"].append((end_time - start_time))
                times["Delta"][i].append(end_time - start_time)
                delta_log["NumberSuboptsPerDelta"][i].append(len([e for e in subopts_i if type(e) is tuple]))
                subopts_temp += [(tuple([nucleotides[j] for j in subopts_i[e][0]]), tuple([babel[j] for j in subopts_i[e][0]]), round(subopts_i[e][1],6), tuple([j for j in subopts_i[e][0]]), M, "Soft_Clashfree") for e in range(len(subopts_i)) if type(subopts_i[e]) is tuple]          
                subopts_temp = list(set(subopts_temp))
                subopts_temp = sorted(subopts_temp, key = lambda x: x[2])
                subopts_temp = subopts_temp[:max_subopts]
                if len(subopts_temp) >= max_subopts:
                    Emax = subopts_temp[max_subopts-1][2]
                delta += step
                if (delta > delta_max) or (delta > Emax-Emfe+step) or (len(subopts_i) == max_subopts):
                    subopts += [(tuple([nucleotides[j] for j in subopts_i[e][0]]), tuple([babel[j] for j in subopts_i[e][0]]), round(subopts_i[e][1],6), tuple([j for j in subopts_i[e][0]]), M, "Soft_Clashfree") for e in range(len(subopts_i)) if type(subopts_i[e]) is tuple]          
                    break
            abs_nb_paths_rejected += subopts_i[-2]
            abs_nb_path_clashfree += subopts_i[-1]
        if i == step_export:      
            temp_log["Iterations/TotalIterations"].append(f"{i}/{M}")
            temp_log["Number of Subopts"].append(len(subopts))
            df_temp_log = pd.DataFrame.from_dict(temp_log)
            df_temp_out = pd.DataFrame([i[:] for i in set(subopts)], columns = ['Sequence', 'PoseSeq', 'EnergySeq', 'Path_GraphNodeIDs', 'TotalNbColoring', 'Clash_Type'])
            df_temp_out.to_csv(f"{output_dir}/{k}nt_subopts_temp.out", sep="\t")
            df_temp_log.to_csv(f"{output_dir}/{k}nt_subopts_temp.log", sep="\t")
            step_export += step_export_init
    count_subopts_with_redundancy = len(subopts)
    subopts = list(set(subopts))
    subopts = sorted(subopts, key = lambda x: x[2])
    return (sum(times["Subopts"]),sum(times["CliquePaths"]),times["Delta"]), subopts, (count_subopts_with_redundancy, abs_nb_path_clashfree, abs_nb_paths_rejected, delta_log["NumberSuboptsPerDelta"])
