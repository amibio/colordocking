'''
FILE    clashesOp.py
DESCR   Python Script to check if it exist a steric clash in a path p
'''

def clashes(p, clash_matrix):
    for i in range(len(p)):
        v = p[i]
        for j in range(i+1, len(p), 1):
            vp = p[j]
            if clash_matrix[v][vp] == 1:
                return 1
    return 0

def clashes_selfavoiding(p):
    k = len(p)
    k_star = len(set(p))
    if k_star != k:
        return 1 
    return 0