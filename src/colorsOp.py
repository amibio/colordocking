'''
FILE    colorsOp.py
DESCR   Python Script to randomly color the graph
'''

def random_colors(len_nodes, k, rnd, cliques=[], use_cliques=True, universal_coloring=False):
    colors_cliques = []
    colors_nodes = []
    if universal_coloring == False:
        if use_cliques == True:
            len_cliques = len(cliques)
            colors_cliques = [rnd.choice(list(range(k))) for i in range(len_cliques)]
            colors_nodes = [0 for i in range(len_nodes)]
            for c in range(len_cliques):
                for j in cliques[c]:
                    colors_nodes[j] = colors_cliques[c]
        else:
            colors_nodes = [rnd.choice(list(range(k))) for i in range(len_nodes)]
    else:
        colors_nodes = [-1 for i in range(len_nodes)]
    return colors_nodes, colors_cliques