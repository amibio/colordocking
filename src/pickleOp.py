import pickle

'''
FILE    pickleOp.py
DESCR   Python Script to pickle and unpickle files
'''

def pickle_from_var(file_name, var):
    with open("{}.pick".format(file_name), 'wb') as file:
        pickle.dump(var, file)

def unpickle_from_file(file_name):
    with open(file_name, 'rb') as file:
        data = pickle.load(file)
    return data