import sys

def sum_dict(s1, s2): 
    res = {k:s1[k] for k in s1} 
    for k in s2: 
        if k in res: 
            res[k] += s2[k] 
        else: res[k] = s2[k] 
    return res 

def mult_dict(s1, x): 
    return {k:s1[k]*x for k in s1}

def meanDict(dic1,factor):
    return {key:round(val/factor,4) for key,val in dic1.items()}

def sum_list(s1,s2):
    if len(s1) == 0 or len(s2) == 0:
        return s1+s2
    return [s1[i]+s2[i] for i in range(len(s1))]