#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#define PY_SSIZE_T_CLEAR
#include <float.h>
#include <math.h>
#include <Python.h>
#include <numpy/arrayobject.h>
#include <stdio.h>
#include <stdlib.h>

// ################ STRUCTURE DECLARATIONS ##################
struct sArray1D_double{
    double *data;
    int size;
};

struct sArray2D_char{
    char **data;
    int size_row;
};

struct sArray2D_double{
    double **data;
    int *size_col;
    int size_row;
};

struct sArray3D_double{
    double ***data;
    int **size_col;
    int size_row;
};

struct sPath{
    double energy;
    int *path;
};

struct abs_count_paths{
    int abs_nb_paths_rejected;
    int abs_nb_paths_clashfree;   
};

// ################ !!! GLOBAL VARIABLE DECLARATIONS !!! ##################

struct sArray1D_double nodesWeight;
struct sArray3D_double linkMatrix;
struct sArray2D_double classifiedNodes;
struct sArray3D_double clashMatrix;
struct sArray2D_char nucleotides;
struct sArray2D_char sequence;
struct sArray1D_double encodedSequence;
int n, k, max_subopts;
int ptr_index_storeArraySubopts;
double delta;

// ################ FUNCTION DECLARATIONS ##################

static PyObject *method_ParserPy2C(PyObject *self, PyObject *args);
static PyObject *method_MemoryRelease(PyObject *self, PyObject *args);
static PyObject *method_MFE(PyObject *self, PyObject *args);
static PyObject *method_Subopts(PyObject *self, PyObject *args);

struct sArray1D_double listDouble2array_1D(PyObject *listObj, struct sArray1D_double sArray1D);
struct sArray2D_double listDouble2array_2D(PyObject **listObj, struct sArray2D_double sArray2D);
struct sArray2D_char listChar2array_2D(PyObject **listObj, struct sArray2D_char sArray2D);
struct sArray3D_double listDouble2array_3D(PyObject ***listObj, struct sArray3D_double sArray3D);

struct sPath MFE_colored(struct sArray1D_double colors, double E[k][n], struct sPath sMFE);
struct sPath MFE_colored_backtrack(int i, int v, double E[k][n], struct sArray1D_double colors, struct sPath sMFE);
void MFE_colored_matrix(struct sArray1D_double colors, double E[k][n]);

struct abs_count_paths subopts_colored_backtrack(int i, int v, double delta, double energy_path, double E[k][n], struct sArray1D_double colors, struct sPath *subopts, int *path_temp, struct abs_count_paths abs_count_paths);
struct abs_count_paths subopts_colored(struct sArray1D_double colors, double E[k][n], struct sPath *subopts);

int indexMax_EnergySubopts(struct sPath *subopts);
int clashes(int *p);
double min_E(double E[k][n], int col);

// ################ FUNCTIONS ##################

static PyObject *method_ParserPy2C(PyObject *self, PyObject *args) {
    // Init. variables
    PyObject ***array_linkMatrix, **array_classifiedNodes, *array_nodesWeight, ***array_clashMatrix, **array_nucleotides, **array_sequence, *array_encodedSeq;

    // Parsing Arguments Python and Checking
    PyArg_ParseTuple(args, "OOiOOOOOi", &array_linkMatrix, &array_classifiedNodes, &k, &array_nodesWeight, &array_clashMatrix, &array_nucleotides, &array_sequence, &array_encodedSeq, &max_subopts);
    if(PyErr_Occurred()){
        return NULL;
    } 
    linkMatrix = listDouble2array_3D(array_linkMatrix, linkMatrix);
    classifiedNodes = listDouble2array_2D(array_classifiedNodes, classifiedNodes);
    clashMatrix = listDouble2array_3D(array_clashMatrix, clashMatrix);
    nodesWeight = listDouble2array_1D(array_nodesWeight, nodesWeight);
    nucleotides = listChar2array_2D(array_nucleotides, nucleotides);
    sequence = listChar2array_2D(array_sequence, sequence);
    encodedSequence =  listDouble2array_1D(array_encodedSeq, encodedSequence);
    n = linkMatrix.size_row;
    Py_RETURN_NONE;

}

static PyObject *method_MemoryRelease(PyObject *self, PyObject *args){
    for(int i=0; i<n; i++){
        for(int j=0; j<classifiedNodes.size_row; j++){
            free(linkMatrix.data[i][j]);
            free(clashMatrix.data[i][j]);
        };
        free(linkMatrix.data[i]);
        free(linkMatrix.size_col[i]);
        free(clashMatrix.data[i]);
        free(clashMatrix.size_col[i]);
        free(nucleotides.data[i]);
    }
    for(int i=0; i<k; i++){
        free(sequence.data[i]);
    }
    for(int i=0; i<classifiedNodes.size_row; i++){
        free(classifiedNodes.data[i]);
    }
    free(linkMatrix.data);
    free(linkMatrix.size_col);
    free(classifiedNodes.data);
    free(classifiedNodes.size_col);
    free(clashMatrix.data);
    free(clashMatrix.size_col);
    free(nodesWeight.data);
    free(nucleotides.data);
    free(sequence.data);
    free(encodedSequence.data);
    Py_RETURN_NONE;
}

static PyObject *method_MFE(PyObject *self, PyObject *args) { // Similar to main() - First executed function in Python
    // Init. variables
    PyObject *array_colors;
    PyObject* PyListPath; // Conversion of array C to Python List;
    PyObject* PyTupleReturn; // Return PyTypleReturn object to Python 
    struct sArray1D_double colors;
    struct sPath sMFE;
    double E[k][n];

    PyArg_ParseTuple(args, "O", &array_colors);
    if(PyErr_Occurred()){
        return NULL;
    }
    colors = listDouble2array_1D(array_colors, colors);

    // Calling C Functions
    int path[k];
    sMFE.path = &path;

    sMFE = MFE_colored(colors, E, sMFE);

    if(sMFE.path != 0){
        PyListPath = PyList_New(k);
        PyTupleReturn = PyTuple_New(2);
        for(int i=0; i<k; i++){
            PyList_SetItem(PyListPath,i,Py_BuildValue("l",sMFE.path[i]));
        }
        PyTuple_SetItem(PyTupleReturn,0,PyListPath);
        PyTuple_SetItem(PyTupleReturn,1,PyFloat_FromDouble(sMFE.energy));
    }
    else{
        PyTupleReturn = PyTuple_New(2);
        PyTuple_SetItem(PyTupleReturn,0,Py_BuildValue(""));
        PyTuple_SetItem(PyTupleReturn,1,PyFloat_FromDouble(sMFE.energy));
    }  

    // Cleaning up memory to avoid memory leaks, and return Python Object
    free(colors.data);
    return PyTupleReturn;
}

static PyObject *method_Subopts(PyObject *self, PyObject *args){ // Similar to main() - First executed function in Python
    // Init. variables 
    PyObject *array_colors;
    PyObject* PyListPath; // Conversion of array C to Python List;
    PyObject* PyTuplePath; // Structure of each Path in each Tuple
    PyObject* PyTupleReturn; // Return PyTypleReturn object to Python
    struct sArray1D_double colors;
    struct sPath subopts[max_subopts];
    double E[k][n];
    ptr_index_storeArraySubopts = 0;

    PyArg_ParseTuple(args, "Od", &array_colors, &delta);
    if(PyErr_Occurred()){
        return NULL;
    }

    colors = listDouble2array_1D(array_colors, colors);
    for(int i=0; i<max_subopts; i++){
        subopts[i].path = (int*)malloc(k * sizeof(int));
        subopts[i].energy = 0.0;
    }

    // Calling C Functions
    struct abs_count_paths abs_count_paths = subopts_colored(colors, E, subopts);
    PyTupleReturn = PyTuple_New(ptr_index_storeArraySubopts+2);
    for(int e=0; e<ptr_index_storeArraySubopts; e++){
        Py_INCREF(Py_None);
        PyTuple_SetItem(PyTupleReturn,e,Py_None);
        PyTuplePath = PyTuple_New(2);
        PyListPath = PyList_New(k);
        for(int i=0; i<k; i++){
            PyList_SetItem(PyListPath,i,PyLong_FromDouble(subopts[e].path[i]));
        }
        PyTuple_SetItem(PyTuplePath,0,PyListPath);
        PyTuple_SetItem(PyTuplePath,1,PyFloat_FromDouble(subopts[e].energy));
        PyTuple_SetItem(PyTupleReturn,e,PyTuplePath);
    }
    PyTuple_SetItem(PyTupleReturn,ptr_index_storeArraySubopts,PyLong_FromDouble(abs_count_paths.abs_nb_paths_rejected)); // Inserting abs_nb_paths_rejected in the last position of the Tuple
    PyTuple_SetItem(PyTupleReturn,ptr_index_storeArraySubopts+1,PyLong_FromDouble(abs_count_paths.abs_nb_paths_clashfree)); // Inserting abs_nb_paths_rejected in the last position of the Tuple
    // Cleaning up memory to avoid memory leaks, and return Python Object
    for(int i=0; i<max_subopts; i++){
        free(subopts[i].path);
    }
    free(colors.data);
    // Return Python Object
    return PyTupleReturn;
}

void MFE_colored_matrix(struct sArray1D_double colors, double E[k][n]){
    int i, j, m, v, vp, nbNeighbors_v, startNuc, endNuc;
    double current_energy;
    for(v=0; v<n; v++){
        if((int)colors.data[v] == k-1 && (strcmp(sequence.data[k-1], "N") == 0 || strcmp(nucleotides.data[v], sequence.data[k-1]) == 0)){
            E[k-1][v] = nodesWeight.data[v];
        }
        else{
            E[k-1][v] = DBL_MAX;
        }
    }
    for(i=(k-2);i>(-1);i--){
        for(v=0; v<n;v++){
            E[i][v] = DBL_MAX;
            if((int)colors.data[v] == i && (strcmp(sequence.data[i], "N") == 0 || strcmp(nucleotides.data[v], sequence.data[i]) == 0)){
                if(strcmp(sequence.data[i+1], "N") == 0){
                    startNuc = 0;
                    endNuc = classifiedNodes.size_row;
                }else{
                    startNuc = (int)encodedSequence.data[i+1];
                    endNuc = (int)encodedSequence.data[i+1]+1;
                }
                for(m=startNuc; m<endNuc; m++){
                    nbNeighbors_v = linkMatrix.size_col[v][m];
                    for(j=0; j<nbNeighbors_v; j++){
                        vp = linkMatrix.data[v][m][j];
                        current_energy = nodesWeight.data[v] + E[i+1][vp];
                        E[i][v] = (E[i][v]<current_energy) ? E[i][v] : current_energy;
                    }
                }
            }
        }
    }
}

struct sPath MFE_colored(struct sArray1D_double colors, double E[k][n], struct sPath sMFE){
    double MFE;
    int startNuc, endNuc, v;
    MFE_colored_matrix(colors, E);
    MFE = min_E(E, 0);
    if(MFE == DBL_MAX){
        sMFE.path = 0;
        sMFE.energy = MFE;
    }
    else{
        if(strcmp(sequence.data[0], "N") == 0){
            startNuc = 0;
            endNuc = classifiedNodes.size_row;
        }else{
            startNuc = (int)encodedSequence.data[0];
            endNuc = (int)encodedSequence.data[0]+1;
        }
        for(int m=startNuc; m<endNuc; m++){
            for(int i=0; i<classifiedNodes.size_col[m]; i++){
                v = classifiedNodes.data[m][i];
                if(E[0][v] == MFE){
                    sMFE = MFE_colored_backtrack(0, v, E, colors, sMFE);
                }
            }
        } 
    }
    return sMFE;
}


struct sPath MFE_colored_backtrack(int i, int v, double E[k][n], struct sArray1D_double colors, struct sPath sMFE){
    double current_energy;
    int m, nbNeighbors_v, vp, startNuc, endNuc;
    if(i == k-1 && (strcmp(sequence.data[k-1], "N") == 0 || strcmp(nucleotides.data[v], sequence.data[k-1]) == 0)){
        sMFE.path[i] = v;
        sMFE.energy = nodesWeight.data[v];
        return sMFE;
    }
    else{
        if((int)colors.data[v] == i && (strcmp(sequence.data[i], "N") == 0 || strcmp(nucleotides.data[v], sequence.data[i]) == 0)){
            if(strcmp(sequence.data[i+1], "N") == 0){
                startNuc = 0;
                endNuc = classifiedNodes.size_row;
            }else{
                startNuc = (int)encodedSequence.data[i+1];
                endNuc = (int)encodedSequence.data[i+1]+1;
            }
            for(m=startNuc; m<endNuc; m++){            
                nbNeighbors_v = linkMatrix.size_col[v][m];
                for(int j=0; j<nbNeighbors_v; j++){
                    vp = linkMatrix.data[v][m][j];
                    current_energy = nodesWeight.data[v] + E[i+1][vp];
                    if(E[i][v] == current_energy){
                        sMFE = MFE_colored_backtrack(i+1, vp, E, colors, sMFE);
                        sMFE.path[i] = v;
                        sMFE.energy += nodesWeight.data[v];
                        return sMFE;
                    }
                }
            }
        } 
    }
}

struct abs_count_paths subopts_colored(struct sArray1D_double colors, double E[k][n], struct sPath *subopts){
    double MFE, delta_p;
    int path_temp[k];
    int startNuc, endNuc, v;
    struct abs_count_paths abs_count_paths;
    abs_count_paths.abs_nb_paths_clashfree = 0;
    abs_count_paths.abs_nb_paths_rejected = 0;

    MFE_colored_matrix(colors, E);
    MFE = min_E(E, 0);
    if(MFE == DBL_MAX){
        return abs_count_paths;
    }
    if(strcmp(sequence.data[0], "N") == 0){
        startNuc = 0;
        endNuc = classifiedNodes.size_row;
    }else{
        startNuc = (int)encodedSequence.data[0];
        endNuc = (int)encodedSequence.data[0]+1;
    }
    for(int m=startNuc; m<endNuc; m++){
        for(int i=0; i<classifiedNodes.size_col[m]; i++){
            v = classifiedNodes.data[m][i];
            delta_p = delta - (E[0][v]-MFE);
            if(delta_p >= 0){
                abs_count_paths = subopts_colored_backtrack(0, v, delta_p, nodesWeight.data[v], E, colors, subopts, path_temp, abs_count_paths);
            }
        }
    }
    return abs_count_paths;    
}

struct abs_count_paths subopts_colored_backtrack(int i, int v, double delta, double energy_path, double E[k][n], struct sArray1D_double colors, struct sPath *subopts, int *path_temp, struct abs_count_paths abs_count_paths){
    double best_energy, current_energy, delta_p;
    int m, nbNeighbors_v, vp, index, startNuc, endNuc; 
    int abs_nb_paths_rejected = 0;
    int abs_nb_paths_clashfree = 0;

    if(i == k-1 && (strcmp(sequence.data[k-1], "N") == 0 || strcmp(nucleotides.data[v], sequence.data[k-1]) == 0)){
        path_temp[i] = v;
        if(clashes(path_temp) == 0){
            if(ptr_index_storeArraySubopts < max_subopts){
                index = ptr_index_storeArraySubopts;
                ptr_index_storeArraySubopts += 1 ;
            }
            else{
                index = indexMax_EnergySubopts(subopts);
            }
            if(energy_path < subopts[index].energy){
                subopts[index].energy = 0.0;
                for(int j=0; j<k; j++){
                    subopts[index].path[j] = path_temp[j];
                    subopts[index].energy += nodesWeight.data[path_temp[j]];
                }                 
            }
            abs_count_paths.abs_nb_paths_clashfree += 1;
            return abs_count_paths;
        }
        abs_count_paths.abs_nb_paths_rejected += 1;
        return abs_count_paths;
    }
    else{
        if((int)colors.data[v] == i && (strcmp(sequence.data[i], "N") == 0 || strcmp(nucleotides.data[v], sequence.data[i]) == 0)){
            path_temp[i] = v;
            best_energy = E[i][v];
            if(strcmp(sequence.data[i+1], "N") == 0){
                startNuc = 0;
                endNuc = classifiedNodes.size_row;
            }else{
                startNuc = (int)encodedSequence.data[i+1];
                endNuc = (int)encodedSequence.data[i+1]+1;
            }
            for(m=startNuc; m<endNuc; m++){             
                nbNeighbors_v = linkMatrix.size_col[v][m];
                for(int j=0; j<nbNeighbors_v; j++){
                    vp = linkMatrix.data[v][m][j];
                    current_energy = nodesWeight.data[v] + E[i+1][vp];
                    delta_p = delta - (current_energy - best_energy);
                    if(delta_p >= 0){
                        abs_count_paths = subopts_colored_backtrack(i+1, vp, delta_p, energy_path+nodesWeight.data[vp], E, colors, subopts, path_temp, abs_count_paths);
                    }
                }
            }
        }
    }
    return abs_count_paths;
}

struct sArray1D_double listDouble2array_1D(PyObject *listObj, struct sArray1D_double sArray1D){
    PyObject *temp;
    int i, col_length;
    col_length = PyList_Size(listObj);
    sArray1D.size = col_length;
    sArray1D.data = (double*)malloc(col_length * sizeof(double));
    for(i=0;i<col_length;i++){
        temp = PyList_GetItem(listObj,i);
        sArray1D.data[i] = PyFloat_AsDouble(temp);
    }
    return sArray1D;
}


struct sArray2D_char listChar2array_2D(PyObject **listObj, struct sArray2D_char sArray2D){
    PyObject *temp;
    int i, j, row_length, col_length;
    row_length = PyList_Size(listObj);
    sArray2D.size_row = row_length;
    sArray2D.data = (char**)malloc(row_length * sizeof(char*));
    for(i=0;i<row_length;i++){
        temp = PyUnicode_AsUTF8String(PyList_GetItem(listObj,i));
        col_length = PyBytes_GET_SIZE(temp)+1;
        sArray2D.data[i] = (char*)malloc(col_length * sizeof(char));
        for(j=0; j<col_length; j++){
            sArray2D.data[i][j] = PyBytes_AsString(temp)[j];
        }
        Py_DECREF(temp);
    }
    return sArray2D;   
}

struct sArray2D_double listDouble2array_2D(PyObject **listObj, struct sArray2D_double sArray2D){
    PyObject *temp;
    PyObject *temp2;
    int i, j, row_length, col_length;
    row_length = PyList_Size(listObj);
    sArray2D.size_row = row_length;
    sArray2D.size_col = (int*)malloc(row_length * sizeof(int));
    sArray2D.data = (double**)malloc(row_length * sizeof(double*));
    for(i=0;i<row_length;i++){
        temp = PyList_GetItem(listObj,i);
        col_length = PyList_Size(temp);
        sArray2D.data[i] = (double*)malloc(col_length *sizeof(double));
        sArray2D.size_col[i] = col_length;
        for(j=0;j<col_length;j++){
            temp2 = PyList_GetItem(temp,j);
            sArray2D.data[i][j] = PyFloat_AsDouble(temp2);
        }
    }
    return sArray2D;
}

struct sArray3D_double listDouble2array_3D(PyObject ***listObj, struct sArray3D_double sArray3D){
    PyObject *temp;
    PyObject *temp2;
    PyObject *temp3;
    int i, j, l, row_length, col_length, col_length2D;
    row_length = PyList_Size(listObj);
    sArray3D.size_row = row_length;
    sArray3D.size_col = (double**)malloc(row_length * sizeof(double));
    sArray3D.data = (double***)malloc(row_length * sizeof(double*));
    for(i=0;i<row_length;i++){
        temp = PyList_GetItem(listObj, i);
        col_length = PyList_Size(temp);
        sArray3D.data[i] = (double**)malloc(col_length *sizeof(double));
        sArray3D.size_col[i] = (int*)malloc(col_length * sizeof(int));
        for(j=0; j<col_length; j++){
            temp2 = PyList_GetItem(temp, j);
            col_length2D = PyList_Size(temp2);
            sArray3D.data[i][j] = (double*)malloc(col_length2D *sizeof(double)); 
            sArray3D.size_col[i][j] = col_length2D;
            for(l=0; l<col_length2D; l++){
                temp3 = PyList_GetItem(temp2,l);
                sArray3D.data[i][j][l] = PyFloat_AsDouble(temp3);
            }
        }      
    }
    return sArray3D;
}


double min_E(double E[k][n], int col){
    int i;
    double min = DBL_MAX;
    for(i=0; i<n; i++){
        min = (min<E[col][i]) ? min : E[col][i];
    }
    return min;
}

int indexMax_EnergySubopts(struct sPath *subopts){
    int i = 0;
    double max = subopts[0].energy;
    for(int e=1; e<max_subopts; e++){
        if(subopts[e].energy > max){
            max = subopts[e].energy;
            i = e;
        }
    }
    return i; 
}

int clashes(int *p){
    int v, vp, code_vp, startNuc, endNuc;
    for(int i=0; i<k; i++){
        v = p[i];
        for(int j=(i+1); j<k; j++){
            vp = p[j];
            code_vp = (int)encodedSequence.data[j];
            if(code_vp == -1){
                startNuc = 0;
                endNuc = classifiedNodes.size_row;
            }
            else{
                startNuc = code_vp;
                endNuc = code_vp+1;
            }
            for(int m = startNuc; m<endNuc; m++){
                for(int l=0; l<clashMatrix.size_col[v][m]; l++){
                    if(vp == clashMatrix.data[v][m][l]){
                        return 1;
                    }
                }
            }

        }
    }
    return 0;
}

/**** Definition Method to call method from Python ****/
static PyMethodDef MFEMethods[] = {
    {"ParserPy2C", method_ParserPy2C, METH_VARARGS, "Function to parse Python arguments to C global variables"},
    {"MemoryRelease", method_MemoryRelease, METH_VARARGS, "Clean memory of global variables"},
    {"MFE", method_MFE, METH_VARARGS, "Function to compute the MFE hard-clashfree"},
    {"Subopts", method_Subopts, METH_VARARGS, "Function to compute the MFE hard-clashfree"},
    {NULL, NULL, 0, NULL}
};


static struct PyModuleDef MFEmodule = {
    PyModuleDef_HEAD_INIT,
    "MFEc",
    "Python interface for the MFE C library function",
    -1,
    MFEMethods
};

PyMODINIT_FUNC PyInit_MFEc() {
    PyObject *module = PyModule_Create(&MFEmodule);
    import_array();
    return module;
}
