import clashesOp
import cliquesOp

def enum_path_rec(G, i, p, k, sequence, nucleotides):
    if i == k:
        return [p]
    list_path = []
    for vp in G.neighbors(p[-1]):
        if (nucleotides[vp] == sequence[i]) or (sequence[i] == "N"):
            list_path += enum_path_rec(G, i+1, p+[vp], k, sequence, nucleotides)
    return list_path
    
def enum_path(G, k, sequence, nucleotides):
    list_path = []
    for v in G.nodes():
        if (nucleotides[v] == sequence[0] or sequence[0] == "N"):
            list_path += enum_path_rec(G, 1, [v], k, sequence, nucleotides)
    return list_path 

def enum_selfavoiding_path(list_path, k, nodes_weight):
    list_selfavoiding_path = {"Paths":[],"Scores":[]}
    for p in list_path:
        if len(set(p)) == k:
            list_selfavoiding_path["Paths"].append(p)
            score = 0
            for n in p:
                score += nodes_weight[n]
            list_selfavoiding_path["Scores"].append(score)
    return list_selfavoiding_path

def enum_selfavoiding_path_cliques(list_path, k, nodes_weight, cliques):
    list_selfavoiding_path_cliques = {"Paths":[],"Scores":[]}
    list_selfavoiding_path = {"Paths":[],"Scores":[]}
    for p in list_path:
        if len(set(p)) == k:
            list_selfavoiding_path["Paths"].append(p)
            score = 0
            for n in p:
                score += nodes_weight[n]
            list_selfavoiding_path["Scores"].append(score)
            if cliquesOp.compatible_paths_cliques(p, cliques) == 0: 
                list_selfavoiding_path_cliques["Paths"].append(p)
                list_selfavoiding_path_cliques["Scores"].append(score)
    return list_selfavoiding_path, list_selfavoiding_path_cliques

def enum_clashfree_path(list_path, clash_matrix, nodes_weight, encodedSeq, cliques):
    list_clashfree_path = {"Paths":[],"Scores":[]}
    list_clashfree_path_cliques = {"Paths":[],"Scores":[]}
    for p in list_path:
        if clashesOp.clashes(p, clash_matrix) == 0:
            list_clashfree_path["Paths"].append(p)
            score = 0
            for n in p:
                score += nodes_weight[n]
            list_clashfree_path["Scores"].append(score)
            if cliquesOp.compatible_paths_cliques(p, cliques) == 0:
                list_clashfree_path_cliques["Paths"].append(p)
                list_clashfree_path_cliques["Scores"].append(score)
    return list_clashfree_path, list_clashfree_path_cliques