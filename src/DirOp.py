import glob
import os
import shutil

'''
FILE    makeDirOp.py
DESCR   Python Script to create automatically the directory of results 
'''

def makeOutputDir(name_dir, cfg):
    i=0
    for d in glob.glob("{}*".format(name_dir)):
        i += 1
    output_dir = "{}".format(name_dir)+"{0:03}".format(i+1)
    os.makedirs(output_dir, exist_ok=False)
    shutil.copyfile(f"{cfg}","{}/{}".format(output_dir,cfg))
    return output_dir
