import pickleOp as pOp
import numpy as np

'''
FILE    cliquesOp.py
DESCR   Python Script to generate cliques from a clash graph G
'''

def existing_selfavoiding_path_cliques_rec(i, p, G_cliques, k, colors):
    if i == k:
        return True
    for vp in G_cliques.neighbors(p[-1]):
        if colors[vp] == i and vp not in p:
            path = existing_selfavoiding_path_cliques_rec(i+1, p+[vp], G_cliques, k, colors)
            if path == True:
                return path 
    return False

def existing_selfavoiding_path_cliques(G_cliques, k, colors):
    path = False
    for v in G_cliques.nodes():
        if colors[v] == 0:
            path = existing_selfavoiding_path_cliques_rec(1, [v], G_cliques, k, colors)
            if path == True:
                break
    return path

def compatible_paths_cliques(path, cliques):
    path_clique = []
    for n in path:
        for c in cliques:
            if n in c:
                if c not in path_clique:
                    path_clique.append(c)
                else:
                    return 1
    return 0

def max_cliques(cliques):
    size_max_cliques = 0
    for c in cliques:
        if len(c) > size_max_cliques:
            size_max_cliques = len(c)
    return size_max_cliques

def get_maximal_clique(G):
    list_degrees = sorted([(len(list(G.neighbors(v))),v) for v in G.nodes],reverse=True)
    if len(list_degrees) == 0:
        return []
    else:
        (degree,start) = list_degrees[0]
        clique = [start]
        candidates = set([v for v in G.neighbors(start) if v != start])
        while len(candidates)>0:
            l = sorted([(len(set(G.neighbors(v)) & candidates),v) for v in candidates],reverse=True)
            (d,v) = l[0]
            clique.append(v)
            candidates = candidates & set([w for w in G.neighbors(v) if w != v])
        return clique

def get_maximal_cliques_cover(G, dir_output):
    G_bck = G.copy()
    res = []
    while len(G_bck.nodes)>0:
        clique = get_maximal_clique(G_bck)
        res.append(clique)
        G_bck.remove_nodes_from(clique)
    pOp.pickle_from_var(f"{dir_output}/cliques", res)
    return res

def get_cliques_matrix(G, cliques):
    cliques_matrix = [[] for i in range(len(cliques))]
    for c in range(len(cliques)):
        for v in cliques[c]:
            for vp in G.neighbors(v):
                if vp not in cliques[c]:
                    id_c_vp = [i for i,j in enumerate(cliques) if vp in j] # In which clique belongs vp ? 
                    if id_c_vp[0] not in cliques_matrix[c]:
                        cliques_matrix[c] += id_c_vp
    return cliques_matrix

def get_matrix_for_nx(matrix):
    matrix_nx = [[0 for i in range(len(matrix))] for j in range(len(matrix))]
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            for m in matrix[i][j]:
                matrix_nx[i][m] = 1
    return np.array(matrix_nx)

def get_cliques_matrix_for_nx(matrix):
    matrix_nx = [[0 for i in range(len(matrix))] for j in range(len(matrix))]
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            matrix_nx[i][j] = 1
    return np.array(matrix_nx)