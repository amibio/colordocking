import numpy as np
import os
import pandas as pd
import pickleOp as pOp
import re
import sys

'''
FILE    TrinucleotidesOp.py
DESCR   Python Script to convert the files from docking of trinucleotides into ColorDocking inputs ; and convert ColorDocking outputs to PDB
'''

def getNucleotideCodes():
    return {"A":0, "C":1, "G":2, "U":3}

def getAtomsOrder_Coords(pdb, dir_output):
    atoms_order = []
    coords = []
    with open(pdb, "r") as f:
        for line in f:
            if ("MODEL" in line):
                atoms_temp = [[] for i in range(3)]
                coords_temp = [[] for i in range(3)]
            elif ("ENDMDL" in line):
                atoms_order.append(atoms_temp)
                coords.append(coords_temp)
            else:
                line = line.split()
                resid = int(line[4])-1
                atm = line[2]
                coords_atm = [float(line[5]), float(line[6]), float(line[7])]
                atoms_temp[resid].append(atm)
                coords_temp[resid].append(coords_atm)
    pOp.pickle_from_var(f"{dir_output}/atoms_order", atoms_order)
    pOp.pickle_from_var(f"{dir_output}/coords", coords)
    return atoms_order, coords
            
def getBabel(npz, dir_output):
    babel = []
    for i in npz["interactions-0"]:
        for j in i:
            babel.append(int(j))
    babel = list(set(babel))
    pOp.pickle_from_var(f"{dir_output}/babel",babel)
    return babel

def getScores(txt, dir_output):
    scores = [float(e) for e in txt]
    pOp.pickle_from_var(f"{dir_output}/scores",scores)
    return scores

def getNucleotides(npz, dir_output):
    nucleotides = [str(e) for e in npz]
    pOp.pickle_from_var(f"{dir_output}/nucleotides",nucleotides)
    return nucleotides

def getClassifiedNodes(nucleotides, dir_output):
    nucleotide_codes = getNucleotideCodes()
    classifiedNodes = [[] for i in range(len(nucleotide_codes))]
    for i,n in enumerate(nucleotides):
        n = n[0]
        classifiedNodes[nucleotide_codes[n]].append(i)
    pOp.pickle_from_var(f"{dir_output}/classifiedNodes",classifiedNodes)
    return classifiedNodes

def getClassifiedNucleotides(nucleotides, dir_output):
    nucleotide_codes = getNucleotideCodes()
    classifiedNucleotides = {}
    for n in nucleotides:
        code_n = nucleotide_codes[n[0]]
        classifiedNucleotides[n] = code_n
    pOp.pickle_from_var(f"{dir_output}/classifiedNucleotides",classifiedNucleotides)
    return classifiedNucleotides

def getCliquesMatrix2D(txt, dir_output):
    cliques = []
    with open(txt, "r") as f:
        for line in f:
            elmnt = [int(i) for i in line.strip("()\n").split(",") if i != '']
            cliques.append(elmnt)
    pOp.pickle_from_var(f"{dir_output}/cliques",cliques)
    return cliques

def getLinkMatrix3D(npz, nbNodes, nucleotides, dir_output):
    link_list = npz["interactions-0"]
    nucleotide_codes = getNucleotideCodes()
    nbNucleotides = len(nucleotide_codes) 
    link_matrix = [[[] for j in range(nbNucleotides)] for i in range(nbNodes)] 
    for elmnt in link_list:
        elmnt0 = int(elmnt[0])
        elmnt1 = int(elmnt[1])
        nuc_elmnt1 = nucleotides[elmnt1][0] # Store the first letter of the code. Ex: AAA -> A ; GUC -> G ...
        code_elmnt = nucleotide_codes[nuc_elmnt1]
        link_matrix[elmnt0][code_elmnt].append(elmnt1)
    pOp.pickle_from_var(f"{dir_output}/link_matrix",link_matrix)
    return link_matrix

def getClashMatrix3D(npy, nbNodes, nucleotides, dir_output):
    nucleotide_codes = getNucleotideCodes()
    nbNucleotides = len(nucleotide_codes) 
    clash_matrix = [[[] for j in range(nbNucleotides)] for i in range(nbNodes)]
    for elmnt in npy:
        elmnt0 = int(elmnt[0])
        elmnt1 = int(elmnt[1])
        nuc_elmnt0 = nucleotides[elmnt0][0]
        code_elmnt0 = nucleotide_codes[nuc_elmnt0]
        nuc_elmnt1 = nucleotides[elmnt1][0]
        code_elmnt1 = nucleotide_codes[nuc_elmnt1]
        clash_matrix[elmnt0][code_elmnt1].append(elmnt1)
        clash_matrix[elmnt1][code_elmnt0].append(elmnt0)
    pOp.pickle_from_var(f"{dir_output}/clash_matrix",clash_matrix)
    return clash_matrix

def parser_MFEout(filename, cutoff):
    df = pd.read_table(filename)
    subopts_paths = []
    for key, value in df.iterrows():
        if key == 0:
            MFE_score = value["EnergySeq"]
            cutoff = MFE_score + cutoff
        else:
            if value["EnergySeq"] > cutoff and cutoff != MFE_score:
                break
            subopts_paths.append([int(i)+1 for i in value["Path_GraphNodeIDs"].strip("()").split(",")]) 
    return subopts_paths


def MFEout2Txt(subopts,filename):
    df = pd.DataFrame(subopts)
    df.to_csv(f"{filename}.txt", sep=" ", index=False, header=False)