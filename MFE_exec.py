import sys
sys.path.insert(0, './src')

import argparse
import configparser
import cliquesOp as cOp
import DirOp as dOp
import MFE
import networkx as nx
import os
import pandas as pd
import pickleOp as pOp
import psutil
import random
import re


##### Parsing Arguments
parser = argparse.ArgumentParser()
parser.add_argument("-config", help="Path to the configuration file")
args = parser.parse_args()

##### Parsing Configuration File
config = configparser.ConfigParser()
config.read(args.config)
config_filename = os.path.basename(args.config) 

# Unpickle pickle files
matrix_dir = config.get("INPUTS", "matrix_dir")
babel = pOp.unpickle_from_file(f"{matrix_dir}/babel.pick")
classifiedNodes = pOp.unpickle_from_file(f"{matrix_dir}/classifiedNodes.pick")
classifiedNucleotides = pOp.unpickle_from_file(f"{matrix_dir}/classifiedNucleotides.pick")
clash_matrix = pOp.unpickle_from_file(f"{matrix_dir}/clash_matrix.pick")
cliques = pOp.unpickle_from_file(f"{matrix_dir}/cliques.pick")
link_matrix = pOp.unpickle_from_file(f"{matrix_dir}/link_matrix.pick")
nodes_weight = pOp.unpickle_from_file(f"{matrix_dir}/scores.pick")
nucleotides = pOp.unpickle_from_file(f"{matrix_dir}/nucleotides.pick")

# Store data from config file into variables
k = int(config.get("PARAMATERS","k"))
delta_max = int(config.get("PARAMATERS","delta_max"))
step = float(config.get("PARAMATERS","step"))
epsilon = float(config.get("PARAMATERS","epsilon"))
use_cliques = config.getboolean("PARAMATERS","use_cliques")
sequence = config.get("SEQUENCE","sequence")
if (sequence == "None") or (sequence == "False"):
    sequence = ['N' for i in range(k)]
    encodedSequence = [-1 for i in range(len(sequence))]
else:
    sequence = re.split("-",sequence)
    encodedSequence = [classifiedNucleotides[i] if i != "N" else -1 for i in sequence]
if k != len(sequence):
    print("K and Size of sequence don't match")
    sys.exit()
seed = config.get("SEED","seed")
if (seed == "None") or (seed == "False"):
    seed = int(random.randrange(sys.maxsize))
else:
    seed = int(seed)
rnd = random.Random(seed)
max_subopts = int(config.get("OUTPUT","maxSubopts_perColoring"))
step_export = int(config.get("OUTPUT","step_export"))


##### PROCESSING
link_matrix_nx = cOp.get_matrix_for_nx(link_matrix)
G_link = nx.from_numpy_array(link_matrix_nx,create_using= nx.DiGraph)

cliques_link_matrix = cOp.get_cliques_matrix(G_link, cliques)
cliques_link_matrix_nx = cOp.get_cliques_matrix_for_nx(cliques_link_matrix)
G_cliques = nx.from_numpy_array(cliques_link_matrix_nx,create_using= nx.DiGraph)

# CREATE OUTPUT DIR
output_dir = dOp.makeOutputDir(f"results_MFE_k{k}_epsilon{epsilon}_delta{delta_max}_", config_filename)

# MFE / Subopts
MFE.MFE_parser(link_matrix, classifiedNodes, k, nodes_weight, clash_matrix, nucleotides, sequence, encodedSequence, max_subopts)
MFE_time, MFE_data = MFE.MFE_Probabilistic_C(link_matrix, G_cliques, k, cliques, nucleotides, babel, epsilon, rnd, output_dir, step_export, use_cliques)
Subopts_time, Subopts_data, Count_Paths = MFE.Subopts_Probabilistic_C(link_matrix, G_cliques, k, cliques, nucleotides, babel, delta_max, step, epsilon, max_subopts, rnd, output_dir, step_export, use_cliques)
MFE.MFE_MemoryRelease()

# Writing Output
totalPoses = len(link_matrix)
cliques_nb = len(cliques)
clique_max = cOp.max_cliques(cliques)

process = psutil.Process(os.getpid())
out = pd.DataFrame([MFE_data], columns = ['Sequence', 'PoseSeq', 'EnergySeq', 'Path_GraphNodeIDs', 'TotalNbColorings', 'Clash_Type'])
out2 = pd.DataFrame([i[:] for i in Subopts_data], columns = ['Sequence', 'PoseSeq', 'EnergySeq', 'Path_GraphNodeIDs', 'TotalNbColorings', 'Clash_Type'])
out = pd.concat([out,out2])
out.to_csv(f"{output_dir}/{k}nt_seq.out", sep="\t")
log_dic = {"Nb_Poses":totalPoses, "Nb_Cliques":cliques_nb, "Size_Maximal_Clique":clique_max, "k":k, "MFE_Time":MFE_time[0], "MFE_CliqueTime":MFE_time[1], "Subopts_Time":Subopts_time[0], "Subopts_CliqueTime":Subopts_time[1], "MFE_Energy":MFE_data[2], "Best_Energy_Subopts":Subopts_data[0][2], "Worst_Energy_Subopts":Subopts_data[-1][2], "Total_Paths_ClashFree":len(Subopts_data), "Total_Paths_ClashFree_Redundancy":Count_Paths[0], "Abs_Total_Paths_ClashFree":Count_Paths[1], "Abs_Total_Paths_Rejected":Count_Paths[2], "Nb_Coloring":MFE_data[4], "Delta":delta_max, "Epsilon":epsilon, "Using_cliques":use_cliques, "Memory_Usage_inBytes":process.memory_info().rss, "Seed":seed}
log = pd.DataFrame(log_dic, index=[0])
log.to_csv(f"{output_dir}/colordocking.log", sep="\t")
delta_times = pd.DataFrame(Subopts_time[2])
delta_times.to_csv(f"{output_dir}/delta_times.log", sep="\t")
delta_subopts = pd.DataFrame(Count_Paths[3])
delta_subopts.to_csv(f"{output_dir}/delta_subopts.log", sep="\t")

