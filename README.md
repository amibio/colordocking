# INSTALL ColorDocking

## Micromamba Environment 

- Install [Micromamba](https://mamba.readthedocs.io/en/latest/installation/micromamba-installation.html)
- Install ColorDocking environment with [colordocking.yml](https://gitlab.inria.fr/amibio/colordocking/-/raw/main/colordocking.yml)
- Activate the colordocking environment 

```
micromamba env create -f colordocking.yml

micromamba activate colordocking
```

## Setup ColorDocking

1. The software includes both C and Python scripts. To compile the code : 

```
CC=gcc python3 setup.py install
```

2. Export in your PYTHONPATH:
 
```
path="$HOME/ColorDocking/"
export PYTHONPATH=${path}/src
```

## Commands to run ColorDocking

```
python3 ${path}/MFE_exec.py -config <file.cfg>
```

To test the well execution of ColorDocking software, we provide the "runtest" folder with expected results. 
