from distutils.core import setup, Extension
import numpy as np

def main():
    module_MFE = Extension("MFEc", sources=["./src/MFE.c"])
    setup(name="ColorDocking_C_Modules",
          version="2.0.0",
          description="",
          author="Taher YACOUB",
          author_email="taher.yacoub@inria.fr",
          ext_modules=[module_MFE],
          include_dirs=[np.get_include()])

if __name__ == "__main__":
    main()
